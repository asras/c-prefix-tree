#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <assert.h>


#define ALPHABETSIZE 25

// ######################
// TRIE TYPE DEF
// ######################
typedef struct Trie {
    char value;
    bool final;
    struct Trie* children;
} Trie;
// ######################
// END TRIE TYPE DEF
// ######################



// ######################
// TRIE UTILITY 
// ######################
#define AS_INT(ch) ((int)ch - (int)'a')
#define NONE '\0'
#define IS_NONE(trie) (trie.value == NONE)
#define CHILD(trie, n) (assert(trie->children != NULL), &trie->children[n])

Trie none_trie() {
    Trie trie = { .value = NONE, .final = false, .children = NULL };
    return trie;
}

Trie empty_trie() {
    Trie root = { .value = NONE, .final = false  };
    root.children = malloc(sizeof(Trie) * ALPHABETSIZE);
    for (int i = 0; i < ALPHABETSIZE; i++) {
        root.children[i] = none_trie();
    }

    return root;
}

void init_trie(Trie *root) {
    root->children = malloc(sizeof(Trie) * ALPHABETSIZE);
    for (int i = 0; i < ALPHABETSIZE; i++) {
        root->children[i] = none_trie();
    }
}

// ######################
// END TRIE UTILITY 
// ######################

// ######################
// TRIE FUNCTIONS 
// ######################
void insert_string(Trie *root, const char* input) {
    int i = 0;
    char current_char = input[i];
    if (AS_INT(current_char) < 0 || AS_INT(current_char) > ALPHABETSIZE) {
        printf("Only lowercase alpha characters are supported");
    }

    Trie *current_node = root;
    while (current_char != '\0') {
        current_node = CHILD(current_node, AS_INT(current_char));
        if (current_node->children == NULL) {
            init_trie(current_node);
        }
        current_node->value = current_char;
        i += 1;
        current_char = input[i];
    }

    current_node->final = true;
}

Trie make_trie(const char *input) {
    Trie root = empty_trie();

    insert_string(&root, input);

    return root;
}

void free_trie(Trie root) {
    if (root.children != NULL) {
        for (int i = 0; i < ALPHABETSIZE; i++) {
            if (!IS_NONE(root.children[i])) {
                free_trie(root.children[i]);
            }
        }

        free(root.children);
    }
}

// Check if trie contains input
bool has_string(Trie root, const char* input) {
    int i = 0;
    char current_char = input[i];
    if (AS_INT(current_char) < 0 || AS_INT(current_char) > ALPHABETSIZE) {
        printf("Only lowercase alpha characters are supported");
    }

    Trie current_node = root;
    while (current_char != '\0') {
        // At leaf node but not at end of input
        if (IS_NONE(current_node.children[AS_INT(current_char)])) {
            return false;
        } 

        current_node = current_node.children[AS_INT(current_char)];
        i += 1;
        current_char = input[i];
    }

    // At end of input, check if this node is a final node 
    return current_node.final;
}
// ######################
// END TRIE FUNCTIONS 
// ######################




// ######################
// DEBUG 
// ######################
void print_trie(Trie root, int level) {
    for (int i = 0; i < level; i++) {
        printf("-");
    }
    if (root.value != '\0') {
        if (root.final) {
            printf("|%c|\n", root.value);
        } else {
            printf("%c\n", root.value);
        }
    }

    if (root.children != NULL) {
        for (int i = 0; i < ALPHABETSIZE; i++) {
            if (!IS_NONE(root.children[i])) {
                print_trie(root.children[i], level + 1);
            }
        }
    }
}
// ######################
// END DEBUG 
// ######################




// ######################
// TEST CODE 
// ######################
// Calculate factorial of n
int fact(int n) {
    if (n == 0) {
        return 1;
    }

    return n * fact(n - 1);
} 

char **combos(const char *input) {
    // Calculate length of null-terminated string
    int length = 0;
    while (input[length] != '\0') {
        length += 1;
    }
    if (length == 0) {
        return NULL;
    }

    // Allocate memory for result
    char **result = malloc(sizeof(char*) * fact(length));
    // Handle corner case where input is a single character
    if (length == 1) {
        result[0] = malloc(sizeof(char) * 2);
        result[0][0] = input[0];
        result[0][1] = '\0';
        return result;
    }

    // Generate every permutation
    for (int i = 0; i < length; i++) {
        // Generate permutations where ith character is first
        char start = input[i];

        // Set up substring that doesn't include the ith character
        char *sub_string = malloc(sizeof(char) * (length));
        for (int j = 0; j < length; j++) {
            if (j < i) {
                sub_string[j] = input[j];
            } else {
                sub_string[j] = input[j + 1];
            }
        }
        sub_string[length - 1] = '\0';

        // Generate permutations of substring
        char **sub_combos = combos(sub_string);
        free(sub_string);

        // Insert the found permutations into result
        int n_sub_combos = fact(length - 1);
        for (int j = 0; j < n_sub_combos; j++) {
            char *current = malloc(sizeof(char) * (length + 1));
            current[0] = start;
            for (int k = 0; k < length - 1; k++) {
                current[k + 1] = sub_combos[j][k];
            }
            current[length] = '\0';
            result[i * n_sub_combos + j] = current;
        }
        free(sub_combos);
    }

    return result;
}

#define test(trie, test_string) printf("Contains %s: %s\n", test_string, has_string(trie, test_string) ? "true" : "false")
#define assert_contains(trie, test_string) assert(has_string(trie, test_string))
#define assert_not_contains(trie, test_string) assert(!has_string(trie, test_string))
// ######################
// END TEST CODE 
// ######################

int main(void) {
#if 0
    Trie root = make_trie("abc");
    insert_string(&root, "def");
    insert_string(&root, "abd");
    insert_string(&root, "abcdefgh");
    insert_string(&root, "abcdetgh");

    print_trie(root, 0);

    printf("TRUE CASES:\n");
    test(root, "abc");
    test(root, "abd");
    test(root, "abcdefgh");

    printf("FALSE CASES:\n");
    test(root, "abe");
    test(root, "abcdetghi");
#else
    Trie root = empty_trie();

    int count = fact(9);
    char **result = combos("abcdefghi");
    char **result2 = combos("ibcdefghi");

    printf("Testing %d combinations\n", count);
    for (int i = 0; i < count; i++) {
        insert_string(&root, result[i]);
    }

    for (int i = 0; i < count; i++) {
        assert_contains(root, result[i]);
    }
    for (int i = 0; i < count; i++) {
        assert_not_contains(root, result2[i]);
    }
    printf("Done");

    free_trie(root);
    free(result);
    free(result2);
#endif

    return 0;
}


